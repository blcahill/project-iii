<!DOCTYPE html>
  <head>
    <meta charset="UTF-8">
    <title> Authentify </title>
    <?php wp_head();?>
  </head>
  <body>
    <div id="header">
      <div id="site-wrapper">
        <div class="site-info">
          <div id="logo">
            <a href="#">Authentify</a>
          </div>
          <div id="call-to-action">
            <a href="http://authentify.com/demo" class="blue-btn">Free Demo</a>
            <p class="phone">+1.773.243.0300</p>
          </div>
      </div>
    </div>

      <div class="nav-menu-wrapper">
        <div id="nav-menu">
        <?php
        wp_nav_menu(array(
          'theme_location' => 'nav-menu'
        ));
        ?>
      </div>
  </div>
</div>


