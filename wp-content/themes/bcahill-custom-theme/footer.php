

      <div id="try-demo">
        <div class="demo-wrapper">
          <?php

          $try_demo_post = get_post(39);
          $try_demo_id = $try_demo_post->ID;

          $details = get_post_meta($try_demo_id, 'Try The Demo', true);?>

          <p><?php print_r($details);?></p>

          <a hre="#" class="blue-btn"><?php print_r($try_demo_post->post_title);?></a>
        </div>
      </div>

      <div id="footer">
        <div class="footer-wrapper">
          <div class="footer-logo">
            <img src="http://authentify.com/wp-content/themes/authentify_childtheme/images/footer-logo.png" alt="Authentify" data-pin-nopin="true">
          </div>
          <div class="contact">
            <?php
            $location_post = get_post(33);
            ?><h1><?php print_r($location_post->post_title);?></h1><?php
            $location_post_id = $location_post->ID;

            //address variable looks to key "Address" in the array and true is gunna return the first value

            $name = get_post_meta($location_post_id, 'Name', true);
            ?><ul>
            <li><h3><?php print_r($name);?><h3/></li><?php
            $address = get_post_meta($location_post_id, 'Address', true);
            ?>
            <li><?php print_r($address);?></li><?php
            $phone = get_post_meta($location_post_id, 'Phone', true);
            ?>
            <li><?php print_r($phone);?></li><?php

            $fax = get_post_meta($location_post_id, 'Fax', true);
            ?>
            <li><?php print_r($fax);?></li><?php
            $email = get_post_meta($location_post_id, 'Email', true);
            ?>
            <li><?php print_r($email);?></li><?php
            //later make email a link or mailto
            ?>
            </ul>
          </div>
          <div class="solutions">
            <?php
            $solution_post = get_post(34);
            ?><h1><?php print_r($solution_post->post_title);?></h1><?php
            $solution_post_id = $solution_post->ID;

            $ecom = get_post_meta($solution_post_id, 'e-Commerce', true);
            ?><ul>
            <li><a href="#"><?php print_r($ecom);?></a></li><?php
            $edu = get_post_meta($solution_post_id, 'Education', true);
            ?>
            <li><a href="#"><?php print_r($edu);?></a></li><?php
            $fin = get_post_meta($solution_post_id, 'Financial Services', true);
            ?>
            <li><a href="#"><?php print_r($fin);?></a></li><?php

            $health = get_post_meta($solution_post_id, 'Healthcare', true);
            ?>
            <li><a href="#"><?php print_r($health);?></a></li><?php
            $secure = get_post_meta($solution_post_id, 'Network Security', true);
            ?>
            <li><a href="#"><?php print_r($secure);?></a></li><?php
            ?>
            </ul>

          </div>
          <div class="learn">
            <?php
            $learn_post = get_post(37);
            ?><h1><?php print_r($learn_post->post_title);?></h1><?php
            $learn_post_id = $learn_post->ID;

            $dwp= get_post_meta($learn_post_id, 'Download White Papers', true);
            ?><ul>
            <li><a href="#"><?php print_r($dwp);?></a></li><?php
            $interact = get_post_meta($learn_post_id, 'Interactive Demos', true);
            ?>
            <li><a href="#"><?php print_r($interact);?></a></li><?php
            $rda = get_post_meta($learn_post_id, 'Request Demo Access', true);
            ?>
            <li><a href="#"><?php print_r($rda);?></a></li><?php

            $vcs = get_post_meta($learn_post_id, 'View Case Studies', true);
            ?>
            <li><a href="#"><?php print_r($vcs);?></a></li><?php

            ?>
            </ul>

          </div>
          <div class="social">
            <?php
            $social_post = get_post(38);
            ?><h1><?php print_r($social_post->post_title);?></h1><?php
            $social_post_id = $social_post->ID;

            $twitter= get_post_meta($social_post_id, 'Follow Us On Twitter', true);
            ?><ul>
            <li><a href="#"><?php print_r($twitter);?></a></li><?php
            $fb = get_post_meta($social_post_id, 'Like Us On Facebook', true);
            ?>
            <li><a href="#"><?php print_r($fb);?></a></li><?php
            $linked_in = get_post_meta($social_post_id, 'Connect On LinkedIn', true);
            ?>
            <li><a href="#"><?php print_r($linked_in);?></a></li><?php

            $blog = get_post_meta($social_post_id, 'Read Our Blog', true);
            ?>
            <li><a href="#"><?php print_r($blog);?></a></li><?php

            $youtube = get_post_meta($social_post_id, 'Watch Us On Youtube', true);
            ?>
            <li><a href="#"><?php print_r($blog);?></a></li>
            </ul>
          </div>
          <div class="compliance">

          </div>
          <div class="copyright">

          </div>
        </div>
      </div>
      </div>
    <?php wp_footer();?>
  </body>
</html>