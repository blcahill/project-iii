<?php
//File of all custom posts in the footer that appears on every page

function custom_footer_post() {

  $labels = array(
    'name'                  => _x( 'Footers Post', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Footer Post', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Footer Post', 'text_domain' ),
    'name_admin_bar'        => __( 'Footer Post', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Footer Post:', 'text_domain' ),
    'all_items'             => __( 'All Footer Post', 'text_domain' ),
    'add_new_item'          => __( 'Add New Footer', 'text_domain' ),
    'add_new'               => __( 'Add Footer Post', 'text_domain' ),
    'new_item'              => __( 'New Footer Post', 'text_domain' ),
    'edit_item'             => __( 'Edit Footer Post', 'text_domain' ),
    'update_item'           => __( 'Update ooter', 'text_domain' ),
    'view_item'             => __( 'View Footer Post', 'text_domain' ),
    'search_items'          => __( 'Search Footer Post', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'items_list'            => __( 'Footer Post list', 'text_domain' ),
    'items_list_navigation' => __( 'Footer Post list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter Footer Posts list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Footer Post', 'text_domain' ),
    'description'           => __( 'Information that will always be in the footer', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'custom-fields',),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-location',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'query_var'             => 'contact_footer',
    'capability_type'       => 'page',
  );
  register_post_type( 'custom_footer_post', $args );

}

// add_action( 'init', 'contact_footer_post', 0 );

// function solution_footer_post() {

//   $labels = array(
//     'name'                  => _x( 'SolutionFooters', 'Post Type General Name', 'text_domain' ),
//     'singular_name'         => _x( 'SolutionFooter', 'Post Type Singular Name', 'text_domain' ),
//     'menu_name'             => __( 'Solution Footer', 'text_domain' ),
//     'name_admin_bar'        => __( 'Solution Footer', 'text_domain' ),
//     'parent_item_colon'     => __( 'Parent Solution Footer:', 'text_domain' ),
//     'all_items'             => __( 'All Solution Footer', 'text_domain' ),
//     'add_new_item'          => __( 'Add New Solution Footer', 'text_domain' ),
//     'add_new'               => __( 'Add Solution Footer', 'text_domain' ),
//     'new_item'              => __( 'New Solution Footer', 'text_domain' ),
//     'edit_item'             => __( 'Edit Solution Footer', 'text_domain' ),
//     'update_item'           => __( 'Update Solution Footer', 'text_domain' ),
//     'view_item'             => __( 'View Solution Footer', 'text_domain' ),
//     'search_items'          => __( 'Search Solution Footer', 'text_domain' ),
//     'not_found'             => __( 'Not found', 'text_domain' ),
//     'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
//     'items_list'            => __( 'Solution Footer list', 'text_domain' ),
//     'items_list_navigation' => __( 'Solution Footer list navigation', 'text_domain' ),
//     'filter_items_list'     => __( 'Filter Solution Footers list', 'text_domain' ),
//   );
//   $args = array(
//     'label'                 => __( 'SolutionFooter', 'text_domain' ),
//     'description'           => __( 'The solution links that will always be in the footer', 'text_domain' ),
//     'labels'                => $labels,
//     'supports'              => array( 'title', 'custom-fields', ),
//     'taxonomies'            => array( 'category', 'post_tag' ),
//     'hierarchical'          => false,
//     'public'                => true,
//     'show_ui'               => true,
//     'show_in_menu'          => true,
//     'menu_position'         => 5,
//     'menu_icon'             => 'dashicons-yes',
//     'show_in_admin_bar'     => true,
//     'show_in_nav_menus'     => true,
//     'can_export'            => true,
//     'has_archive'           => true,
//     'exclude_from_search'   => false,
//     'publicly_queryable'    => true,
//     'query_var'             => 'solution_footer',
//     'capability_type'       => 'page',
//   );
//   register_post_type( 'solution_footer', $args );

// }
// add_action( 'init', 'solution_footer_post', 0 );

// function learn_more_footer_post() {

//   $labels = array(
//     'name'                  => _x( 'LearnMoreFooters', 'Post Type General Name', 'text_domain' ),
//     'singular_name'         => _x( 'LearnMoreFooter', 'Post Type Singular Name', 'text_domain' ),
//     'menu_name'             => __( 'Learn More Footer', 'text_domain' ),
//     'name_admin_bar'        => __( 'Learn More Footer', 'text_domain' ),
//     'parent_item_colon'     => __( 'Parent Learn More Footer:', 'text_domain' ),
//     'all_items'             => __( 'All Learn More Footer', 'text_domain' ),
//     'add_new_item'          => __( 'Add New Learn More Footer', 'text_domain' ),
//     'add_new'               => __( 'Add Learn More Footer', 'text_domain' ),
//     'new_item'              => __( 'New Learn More Footer', 'text_domain' ),
//     'edit_item'             => __( 'Edit Learn More Footer', 'text_domain' ),
//     'update_item'           => __( 'Update Learn More Footer', 'text_domain' ),
//     'view_item'             => __( 'View Learn More Footer', 'text_domain' ),
//     'search_items'          => __( 'Search Learn More Footer', 'text_domain' ),
//     'not_found'             => __( 'Not found', 'text_domain' ),
//     'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
//     'items_list'            => __( 'Learn More Footer list', 'text_domain' ),
//     'items_list_navigation' => __( 'Learn More Footer list navigation', 'text_domain' ),
//     'filter_items_list'     => __( 'Filter Learn More Footers list', 'text_domain' ),
//   );
//   $args = array(
//     'label'                 => __( 'LearnMoreFooter', 'text_domain' ),
//     'description'           => __( 'The provide links for clients to discover more and that will always be in the footer', 'text_domain' ),
//     'labels'                => $labels,
//     'supports'              => array( 'title', 'custom-fields', ),
//     'taxonomies'            => array( 'category', 'post_tag' ),
//     'hierarchical'          => false,
//     'public'                => true,
//     'show_ui'               => true,
//     'show_in_menu'          => true,
//     'menu_position'         => 5,
//     'menu_icon'             => 'dashicons-lightbulb',
//     'show_in_admin_bar'     => true,
//     'show_in_nav_menus'     => true,
//     'can_export'            => true,
//     'has_archive'           => true,
//     'exclude_from_search'   => false,
//     'publicly_queryable'    => true,
//     'query_var'             => 'learn_more_footer',
//     'capability_type'       => 'page',
//   );
//   register_post_type( 'learn_more_footer', $args );

// }
// add_action( 'init', 'learn_more_footer_post', 0 );

// function follow_us_footer_post() {

//   $labels = array(
//     'name'                  => _x( 'FollowUsFooters', 'Post Type General Name', 'text_domain' ),
//     'singular_name'         => _x( 'FollowUsFooter', 'Post Type Singular Name', 'text_domain' ),
//     'menu_name'             => __( 'Follow Us Footer', 'text_domain' ),
//     'name_admin_bar'        => __( 'Follow Us Footer', 'text_domain' ),
//     'parent_item_colon'     => __( 'Parent Follow Us Footer:', 'text_domain' ),
//     'all_items'             => __( 'All Follow Us Footer', 'text_domain' ),
//     'add_new_item'          => __( 'Add New Follow Us Footer', 'text_domain' ),
//     'add_new'               => __( 'Add Follow Us Footer', 'text_domain' ),
//     'new_item'              => __( 'New Follow Us Footer', 'text_domain' ),
//     'edit_item'             => __( 'Edit Follow Us Footer', 'text_domain' ),
//     'update_item'           => __( 'Update Follow Us Footer', 'text_domain' ),
//     'view_item'             => __( 'View Follow Us Footer', 'text_domain' ),
//     'search_items'          => __( 'Search Follow Us Footer', 'text_domain' ),
//     'not_found'             => __( 'Not found', 'text_domain' ),
//     'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
//     'items_list'            => __( 'Follow Us Footer list', 'text_domain' ),
//     'items_list_navigation' => __( 'Follow Us Footer list navigation', 'text_domain' ),
//     'filter_items_list'     => __( 'Filter Follow Us Footers list', 'text_domain' ),
//   );
//   $args = array(
//     'label'                 => __( 'FollowUsFooter', 'text_domain' ),
//     'description'           => __( 'The provide links for clients to connect withs us and that will always be in the footer', 'text_domain' ),
//     'labels'                => $labels,
//     'supports'              => array( 'title', 'custom-fields', ),
//     'taxonomies'            => array( 'category', 'post_tag' ),
//     'hierarchical'          => false,
//     'public'                => true,
//     'show_ui'               => true,
//     'show_in_menu'          => true,
//     'menu_position'         => 5,
//     'menu_icon'             => 'dashicons-groups',
//     'show_in_admin_bar'     => true,
//     'show_in_nav_menus'     => true,
//     'can_export'            => true,
//     'has_archive'           => true,
//     'exclude_from_search'   => false,
//     'publicly_queryable'    => true,
//     'query_var'             => 'follow_us_footer',
//     'capability_type'       => 'page',
//   );
//   register_post_type( 'follow_us_footer', $args );

// }
// add_action( 'init', 'follow_us_footer_post', 0 );
?>

