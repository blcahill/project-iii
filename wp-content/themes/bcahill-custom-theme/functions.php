<?php

//require all php files that hold custom posts in php
require_once("custom/custom-footer-posts.php");
//requiring ends here

//getting style.css up and running
function custom_css_include()
{

//prevents from showing up in backend?
if( $GLOBALS["pagenow"] == "wp-login.php" || is_admin() ) { return false; }

  wp_register_style(
    "custom-css",
    get_stylesheet_directory_uri()."/style.css",
    array(),
    "20121207",
    "all");

  wp_enqueue_style("custom-css");
}

add_action("wp_enqueue_scripts", "custom_css_include");


// get homepage.js up and running on wp
function custom_js_include()
{
  wp_register_script(
  "custom-js",
  get_stylesheet_directory_uri()."/homepage.js",
  array('jquery'),
  '20151207',
  true
  );
  wp_enqueue_script('custom-js');
}

add_action("wp_enqueue_scripts","custom_js_include");

function theme_init()
{
  register_nav_menus(array(
      'nav-menu' => 'Nav Menu'
    ));
}

add_action('init', 'theme_init');


function register_custom_post_types()
{
  custom_footer_post();
}

add_action( 'init', 'register_custom_post_types', 0);

?>


