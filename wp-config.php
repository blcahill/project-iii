<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mock-authentify');

/** MySQL database username */
define('DB_USER', 'authentify-user');

/** MySQL database password */
define('DB_PASSWORD', 'aKQAxTtsRYUK73Se');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#&-~q]R9XMNr+}M0QxNo}#)Yd1WAi7q}FmKUB}#Si#;}Aok/is2Yi~^^+D,<)*~s');
define('SECURE_AUTH_KEY',  'BWo7<)z<MZM ExeuoQPytWBd++.)sI[|M&*M[)9:sxgN^6>0^zj **}MKW+Gdp8q');
define('LOGGED_IN_KEY',    'tC=Mn<uO{J?+iB -8mY$,r$Ibbdw[=(FdWi+5HtlD{~zB(:hOT%jNxb1U0Dv#u|c');
define('NONCE_KEY',        '&^xeA)hr[%be4l[]s$txhOScK|abHdH++3,wW~e?-sE;-HXR7#8lDZe*r5e}I=j<');
define('AUTH_SALT',        'W)Z`!ewJ|JAjKs#?4_yQ:O[^1*qTAV??<N;Y~-,J^pTf4{X#gR{|u-p Yq<5u;!W');
define('SECURE_AUTH_SALT', ' CmYs<~.zB9l7f(d$=<g<$I<TFdADdE<J[Y-)1P;0$OGa*Td2,kCov7B},9EU>^>');
define('LOGGED_IN_SALT',   'M6?hk$Hw%4f&TBKp+`[N}MguT9fX4>%{Ur^-6E$x(:EaHR{8;qZ#4aE[<Z8y~h%5');
define('NONCE_SALT',       'wsb7.(|B>,].BrqPah,q+dzMksjkeXWV,#~nf{E%UHK[z.V&Bb;._C[X@7geQ]*2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
